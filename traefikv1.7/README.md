

https://supergiant.io/blog/using-traefik-as-ingress-controller-for-your-kubernetes-cluster/



```bash
# setup service account name
kubectl create -f rbac/traefik-service-acc.yaml
# create the clusterRole
kubectl create -f rbac/traefik-cr.yaml
# create the binding of the clusterRole to the service account
kubectl create -f rbac/traefik-crb.yaml 
# do the traefik deployment
kubectl create -f deployment/traefik-deployment.yaml

# now check the PODS are running 
kubectl --namespace=kube-system get pods

# now deploy the service as a NodePort
kubectl create -f service/traefik-svc.yaml
#Now, let’s verify that the Service was created:
kubectl describe svc traefik-ingress-service --namespace=kube-system

#now create the traefik webui service
kubectl create -f service/traefik-webui-svc.yaml
#Let’s verify that the Service was created:
kubectl describe svc traefik-web-ui --namespace=kube-system



kubectl create -f ingress/traefik-ingress.yaml
```
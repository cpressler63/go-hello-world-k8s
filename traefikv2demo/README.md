

https://docs.traefik.io/user-guides/crd-acme/

####Cluster Resources¶
Let's now have a look (in the order they should be applied, if using kubectl apply) at all the required resources for the full setup.

####IngressRoute Definition¶
First, the definition of the IngressRoute and the Middleware kinds. Also note the RBAC authorization resources; they'll be referenced through the serviceAccountName of the deployment, later on.   

**prereqs.yaml**  

####Services¶
Then, the services. One for Traefik itself, and one for the app it routes for, i.e. in this case our demo HTTP server: whoami.   

**service.yaml**

####Deployments¶
Next, the deployments, i.e. the actual pods behind the services. Again, one pod for Traefik, and one for the whoami app.  

**deployment.yaml**

####Port Forwarding¶
Now, as an exception to what we said above, please note that you should not let the ingressRoute resources below be applied automatically to your cluster. The reason is, as soon as the ACME provider of Traefik detects we have TLS routers, it will try to generate the certificates for the corresponding domains. And this will not work, because as it is, our Traefik pod is not reachable from the outside, which will make the ACME TLS challenge fail. Therefore, for the whole thing to work, we must delay applying the ingressRoute resources until we have port-forwarding set up properly, which is the next step.

```bash
kubectl port-forward --address 0.0.0.0 service/traefik 8000:8000 8080:8080 443:4443 -n default
```
Also, and this is out of the scope if this guide, please note that because of the privileged ports limitation on Linux, the above command might fail to listen on port 443. In which case you can use tricks such as elevating caps of kubectl with setcaps, or using authbind, or setting up a NAT between your host and the WAN. Look it up.

Traefik Routers¶
We can now finally apply the actual ingressRoutes, with:


kubectl apply -f ingressroutes.yaml

####ingressroutes.yaml

Give it a few seconds for the ACME TLS challenge to complete, and you should then be able to access your whoami pod (routed through Traefik), from the outside. Both with or (just for fun, do not do that in production) without TLS:


curl [-k] https://your.domain.com/tls

curl [-k] http://your.domain.com:8000/notls
Note that you'll have to use -k as long as you're using the staging server of Let's Encrypt, since it is not an authorized certificate authority on systems where it hasn't been manually added.

**Create Kubernetes resources Sequence**

```bash
kubectl apply -f prereqs.yaml

kubectl apply -f service.yaml
kubectl apply -f deployment.yaml
# in another terminal 
kubectl port-forward --address 0.0.0.0 service/traefik 8000:8000 8080:8080 443:4443 -n default
# now you can goto localhost:8080 and see the traefik admin interface

kubectl apply -f ingressroutes.yaml
# add entry 127.0.0.1 for testk8s.pressler.com in to /etc/hosts

curl -k https://testk8s.pressler.com/tls
curl -k http://testk8s.pressler.com:8000/tls

# test the local IP as well
curl -k --header "Host: testk8s.pressler.com" https://192.168.102.38/tls
```
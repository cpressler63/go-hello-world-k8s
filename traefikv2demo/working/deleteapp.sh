#!/bin/sh

#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" get nodes
#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" get pods
#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" get services
#
#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" delete -f traefik-ingress.yaml
#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" delete -f service.yaml
#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" delete -f deployment.yaml
#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" delete -f prereqs.yaml
#
#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" get nodes
#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" get pods
#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" get services


kubectl  get nodes
kubectl  get pods
kubectl  get services

kubectl  delete -f traefik-ingress-split.yaml
kubectl  delete -f traefik-middlewares.yaml
kubectl delete -f service-lb.yaml
kubectl  delete -f deployment.yaml
kubectl  delete -f prereqs.yaml

kubectl  get nodes
kubectl  get pods
kubectl   get services


#!/bin/sh

#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" get nodes
#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" get pods
#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" get services
#
#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" apply -f prereqs.yaml
#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" apply -f deployment.yaml
#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" apply -f service.yaml
##kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" apply -f traefik-ingress.yaml
#
#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" get nodes
#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" get pods
#kubectl --kubeconfig="/Users/chesterpressler/.kube/k8s-vb-config.yaml" get services


kubectl  get nodes
kubectl  get pods
kubectl  get services

kubectl  apply -f prereqs.yaml
kubectl  apply -f deployment.yaml
kubectl apply -f service-lb.yaml
kubectl  apply -f traefik-middlewares.yaml
kubectl  apply -f traefik-ingress-split.yaml

kubectl  get nodes
kubectl  get pods
kubectl   get services